---
layout: page
title: Blog
permalink: /blog/
---

<div class="main-post-list">
  {% for post in site.categories['blog'] %}
    <article class="main-post-list">
      <h1 class="post-list__post-title post-title">
          <a href="{{ site.baseurl }}{{ post.url }}">{{ post.title }}</a>
      </h1>
      <div class="entry">
        {{ post.excerpt }}
      </div>
      <div class="post-list__meta">
          <time datetime="{{ post.date | date: '%Y-%m-%d %H:%M' }}" class="post-list__meta--date date">{{ post.date | date: "%-d %b %Y" }}</time>
          {% if post.tags.size > 0 %}
          &#8226; <span class="post-meta__tags">on {% for tag in post.tags %}<a href="{{ site.baseurl }}/tags/#{{ tag }}">{{ tag }}</a>{% if forloop.last == false %}, {% endif %}{% endfor %}</span>
          {% endif %}
      </div>
      <hr class="post-list__divider">
    </article>
  {% endfor %}
</div>