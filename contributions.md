---
layout: page
title: Contributions
permalink: /contributions/
---

<div class="main-post-list">
  <h2 class="post-list__post-title post-title">Contributions</h2>
        - <a href="https://github.com/rhythmictech/terraform-aws-nlb-ecs-task/pulls?q=is%3Apr+author%3Acvegagimenez" title="terraform-aws-nlb-ecs-task">terraform-aws-nlb-ecs-task</a>: 
        <p style="text-indent:20px"><a href="https://registry.terraform.io/modules/rhythmictech/nlb-ecs-task/aws/latest" title="terraform-aws-nlb-ecs-task">Terraform module</a> to manage ECS tasks and NLBs.</p>
        - <a href="https://github.com/VirtualCave/containers-lab" title="containers-lab">containers-lab</a>: 
        <p style="text-indent:20px">Introduction to containers and Kubernetes.</p>
        - <a href="https://github.com/mittwald/goharbor-client/pulls?q=is%3Apr+author%3Acvegagimenez" title="goharbor-client">goharbor-client</a>: 
        <p style="text-indent:20px">A Go client enabling programs to perform CRUD operations on the <a href="https://github.com/goharbor/harbor" title="goharbor-client">go-harbor</a> API.</p>
        - <a href="https://github.com/goharbor/harbor-helm/pulls?q=is%3Apr+author%3Acvegagimenez" title="harbor-helm">harbor-helm</a>: 
        <p style="text-indent:20px">Harbor Helm Chart.</p>
        - <a href="https://github.com/sigstore/helm-charts/pulls?q=is%3Apr+author%3Acvegagimenez" title="sigstore-helm">sigstore-helm</a>: 
        <p style="text-indent:20px">Sigstore Helm Chart.</p>
  <h2 class="post-list__post-title post-title">Talks</h2>
        - <a href="https://www.youtube.com/watch?v=SJDFjC4ao7U&t=19892s" title="BK8S-Day-2021">BK8S Day 2021</a>: 
        <p style="text-indent:20px">At the gates of Kubernetes (Spanish)</p>
