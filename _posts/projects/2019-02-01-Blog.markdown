---
title:  "Blog"
date:   2019-02-01 11:00:00
categories: [projects]
tags: [blog, jekyll, project]
---

![pipeline](https://gitlab.com/Gigigotrek/cvega/badges/master/pipeline.svg?key_text=blog&key_width=40)

This is the project where I develop this blog that you are reading right now. It works by using the static sites generator called [Jekyll](https://jekyllrb.com/) and wrote in Ruby. This service allows to create a simple website just by using Markdown files. 

This blog is published in [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) and to make all easier, I have created a Continuous Deployment process for the project using Gitlab-CI and it will allow to deploy the website automatically with every merge in Master branch. 

[Check it out](https://gitlab.com/Gigigotrek/cvega) here.
