# Carlos Vega blog

## Description

In this project I am going to post stuff mainly about new technologies like DevOps, Development, 3D printing...

This blog is created with Jekyll as static content website generator and will be posted on Gitlab Pages through [https://gigigotrek.gitlab.io/cvega/](https://gigigotrek.gitlab.io/cvega/).

### How to test locally 

1. Download or clone repo `git clone git@gitlab.com:Gigigotrek/cvega.git`
2. Enter the folder: `cd cvega/`
3. If you don't have bundler installed: `gem install bundler`
3. Install Ruby gems: `bundle install`
4. Start Jekyll server: `bundle exec jekyll serve --watch`

Access via: [http://127.0.0.1:4000](http://127.0.0.1:4000)


### Credits, Copyright and License

Based on [Jekyll-Uno](https://github.com/joshgerdes/jekyll-uno)

It is under [the MIT license](/LICENSE).